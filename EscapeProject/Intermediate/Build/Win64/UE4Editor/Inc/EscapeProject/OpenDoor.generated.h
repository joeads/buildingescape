// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ESCAPEPROJECT_OpenDoor_generated_h
#error "OpenDoor.generated.h already included, missing '#pragma once' in OpenDoor.h"
#endif
#define ESCAPEPROJECT_OpenDoor_generated_h

#define EscapeProject_Source_EscapeProject_OpenDoor_h_13_SPARSE_DATA
#define EscapeProject_Source_EscapeProject_OpenDoor_h_13_RPC_WRAPPERS
#define EscapeProject_Source_EscapeProject_OpenDoor_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define EscapeProject_Source_EscapeProject_OpenDoor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOpenDoor(); \
	friend struct Z_Construct_UClass_UOpenDoor_Statics; \
public: \
	DECLARE_CLASS(UOpenDoor, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EscapeProject"), NO_API) \
	DECLARE_SERIALIZER(UOpenDoor)


#define EscapeProject_Source_EscapeProject_OpenDoor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUOpenDoor(); \
	friend struct Z_Construct_UClass_UOpenDoor_Statics; \
public: \
	DECLARE_CLASS(UOpenDoor, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EscapeProject"), NO_API) \
	DECLARE_SERIALIZER(UOpenDoor)


#define EscapeProject_Source_EscapeProject_OpenDoor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOpenDoor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOpenDoor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOpenDoor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOpenDoor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOpenDoor(UOpenDoor&&); \
	NO_API UOpenDoor(const UOpenDoor&); \
public:


#define EscapeProject_Source_EscapeProject_OpenDoor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOpenDoor(UOpenDoor&&); \
	NO_API UOpenDoor(const UOpenDoor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOpenDoor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOpenDoor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UOpenDoor)


#define EscapeProject_Source_EscapeProject_OpenDoor_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MassToOpenDoors() { return STRUCT_OFFSET(UOpenDoor, MassToOpenDoors); } \
	FORCEINLINE static uint32 __PPO__TargetYaw() { return STRUCT_OFFSET(UOpenDoor, TargetYaw); } \
	FORCEINLINE static uint32 __PPO__PressurePlate() { return STRUCT_OFFSET(UOpenDoor, PressurePlate); } \
	FORCEINLINE static uint32 __PPO__AudioComponent() { return STRUCT_OFFSET(UOpenDoor, AudioComponent); }


#define EscapeProject_Source_EscapeProject_OpenDoor_h_10_PROLOG
#define EscapeProject_Source_EscapeProject_OpenDoor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EscapeProject_Source_EscapeProject_OpenDoor_h_13_PRIVATE_PROPERTY_OFFSET \
	EscapeProject_Source_EscapeProject_OpenDoor_h_13_SPARSE_DATA \
	EscapeProject_Source_EscapeProject_OpenDoor_h_13_RPC_WRAPPERS \
	EscapeProject_Source_EscapeProject_OpenDoor_h_13_INCLASS \
	EscapeProject_Source_EscapeProject_OpenDoor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EscapeProject_Source_EscapeProject_OpenDoor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EscapeProject_Source_EscapeProject_OpenDoor_h_13_PRIVATE_PROPERTY_OFFSET \
	EscapeProject_Source_EscapeProject_OpenDoor_h_13_SPARSE_DATA \
	EscapeProject_Source_EscapeProject_OpenDoor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	EscapeProject_Source_EscapeProject_OpenDoor_h_13_INCLASS_NO_PURE_DECLS \
	EscapeProject_Source_EscapeProject_OpenDoor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ESCAPEPROJECT_API UClass* StaticClass<class UOpenDoor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EscapeProject_Source_EscapeProject_OpenDoor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
