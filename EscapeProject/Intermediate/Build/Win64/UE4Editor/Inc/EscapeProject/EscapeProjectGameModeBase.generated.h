// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ESCAPEPROJECT_EscapeProjectGameModeBase_generated_h
#error "EscapeProjectGameModeBase.generated.h already included, missing '#pragma once' in EscapeProjectGameModeBase.h"
#endif
#define ESCAPEPROJECT_EscapeProjectGameModeBase_generated_h

#define EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_SPARSE_DATA
#define EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_RPC_WRAPPERS
#define EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEscapeProjectGameModeBase(); \
	friend struct Z_Construct_UClass_AEscapeProjectGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AEscapeProjectGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EscapeProject"), NO_API) \
	DECLARE_SERIALIZER(AEscapeProjectGameModeBase)


#define EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAEscapeProjectGameModeBase(); \
	friend struct Z_Construct_UClass_AEscapeProjectGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AEscapeProjectGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EscapeProject"), NO_API) \
	DECLARE_SERIALIZER(AEscapeProjectGameModeBase)


#define EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEscapeProjectGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEscapeProjectGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEscapeProjectGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEscapeProjectGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEscapeProjectGameModeBase(AEscapeProjectGameModeBase&&); \
	NO_API AEscapeProjectGameModeBase(const AEscapeProjectGameModeBase&); \
public:


#define EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEscapeProjectGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEscapeProjectGameModeBase(AEscapeProjectGameModeBase&&); \
	NO_API AEscapeProjectGameModeBase(const AEscapeProjectGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEscapeProjectGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEscapeProjectGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEscapeProjectGameModeBase)


#define EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_12_PROLOG
#define EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_SPARSE_DATA \
	EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_RPC_WRAPPERS \
	EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_INCLASS \
	EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_SPARSE_DATA \
	EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ESCAPEPROJECT_API UClass* StaticClass<class AEscapeProjectGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EscapeProject_Source_EscapeProject_EscapeProjectGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
